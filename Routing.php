<?php

require_once 'Controllers//BoardController.php';
require_once 'Controllers//SecurityController.php';
require_once 'Controllers//AdminController.php';
require_once 'Controllers//UsersController.php';

class Routing {
    private $routes = [];

    public function __construct()
    {
        $this->routes = [
            'board' => [
                'controller' => 'BoardController',
                'action' => 'yourboard'
            ],
            'login' => [
                'controller' => 'SecurityController',
                'action' => 'login'
            ],
            'logout' => [
                'controller' => 'SecurityController',
                'action' => 'logout'
            ],
            'admin' => [
                'controller' => 'AdminController',
                'action' => 'index'
            ],
            'users' => [
                'controller' => 'AdminController',
                'action' => 'users'
            ],
            'delatebyid' => [
                'controller' => 'AdminController',
                'action' => 'delatebyid'
            ],
            'registration' => [
                'controller' => 'SecurityController',
                'action' => 'registration'
            ],
            'userpanel' => [
                'controller' => 'UsersController',
                'action' => 'index'
            ],
            'usergoal' => [
                'controller' => 'UsersController',
                'action' => 'index2'
            ],
            'userpassword' => [
                'controller' => 'UsersController',
                'action' => 'index3'
            ],
            'addexpenses' => [
                'controller' => 'BoardController',
                'action' => 'index'
            ],
            'addexp' => [
                'controller' => 'BoardController',
                'action' => 'addexp'
            ],
            'patchdata' => [
                'controller' => 'UsersController',
                'action' => 'patchdata'
            ],
            'patchgoal' => [
                'controller' => 'UsersController',
                'action' => 'patchgoal'
            ],
            'patchpass' => [
                'controller' => 'UsersController',
                'action' => 'patchpass'
            ],
            'user_contr'=>[
                'controller' => 'UsersController',
                'action' => 'wait'
            ],
        ];
    }

    public function run()
    {
        $page = isset($_GET['page']) ? $_GET['page'] : 'login';

        if (isset($this->routes[$page])) {
            $controller = $this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            $object = new $controller;
            $object->$action();
        }
    }
}