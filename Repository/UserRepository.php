<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//User.php';

class UserRepository extends Repository {

    public function getUser(string $email): ?User 
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM users inner join USERS_DETAILS on users.id=USERS_DETAILS.USER_ID WHERE email = :email
        ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false) {
            return null;
        }

        return new User(
            $user['email'],
            $user['password'],
            $user['name'],
            $user['surname'],
            $user['id'],
            $user['role'],
            $user['AGE'],
            $user['CITY'],
            $user['PROFESSION'],
            $user['PHONE'],
            $user['NICK']
        );
    }

    public function getUsers(): array {
        // $result = [];
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM users WHERE email != :email;
        ');
        $stmt->bindParam(':email', $_SESSION['id'], PDO::PARAM_STR);
        $stmt->execute();
        $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // foreach ($users as $user) {
        //     $result[] = new User(
        //         $user['email'],
        //         $user['password'],
        //         $user['name'],
        //         $user['surname'],
        //         $user['id']
        //     );
        // }

        return $users;
    }
    public function newUsers(string $email, string $password,string $name="0", string $surname="0" ){
        $stmt = $this->database->connect()->prepare('
            INSERT INTO users (email,password,name,surname,role) values(?,?,?,?,"user");
        ');
        $stmt->execute([$email, $password, $name,$surname]);
        $stmt = $this->database->connect()->prepare('
            Select id from users where email=?;
        ');
        $stmt->execute([$email]);
        $users = $stmt->fetch(PDO::FETCH_ASSOC);
        $id = $users['id'];
        $stmt = $this->database->connect()->prepare('
        INSERT INTO USERS_DETAILS (USER_ID,AGE,CITY,PROFESSION,PHONE,NICK) VALUES (?,"0","0","0","0","0");
        ');
        $stmt->execute([$id]);
        $stmt = $this->database->connect()->prepare('
        INSERT INTO USER_BUSSINESS_GOAL (USER_ID,GOAL,DATE_MONTH,DATE_YEAR) VALUES (?,\'1000\' ,EXTRACT(MONTH FROM SYSDATE()),EXTRACT(YEAR FROM SYSDATE()));  
        ');
        $stmt->execute([$id]);
        $stmt = $this->database->connect()->prepare('
        INSERT INTO USER_EXPENSES (USER_ID,DATE_EXP,VALUE_EXP,COMMENT) VALUES (?,SYSDATE(),\'0\',"TEST_COMMENT");  
        ');
        $stmt->execute([$id]);

    }
    public function GetUsersName(int $Id): string{
        $stmt = $this->database->connect()->prepare('
            SELECT name FROM users WHERE id=?;');
        $stmt->execute([$Id]);
        return $stmt->fetch();
    }
    public function patchdate(string $id,String $email,String $name,String $surname,
    String $age,String $City,String $profession,String $phone,String $nick)
    {
        
        try {
        $stmt = $this->database->connect()->prepare('
        UPDATE users 
        SET email=?,name=?,surname=?
        WHERE id=?;
    ');
    $stmt->execute([$email, $name, $surname,$id]);
        }
        catch(PDOException $e)
        {
    echo $stmt  . "<br>" . $e->getMessage();
        }
    $stmt = null;

    try {
    $stmt = $this->database->connect()->prepare('
    UPDATE USERS_DETAILS 
    SET AGE=?,CITY=?,PROFESSION=?,PHONE=?,NICK=?
    WHERE USER_ID=?;
');
$stmt->execute([$age, $City,$profession, $phone,$nick,$id]);
}
catch(PDOException $e)
{
echo $stmt  . "<br>" . $e->getMessage();
}    
}

public function patchgoal(string $id,string $goal){
    
    $goal=floatval($goal);
    try {
        $stmt = $this->database->connect()->prepare('
        UPDATE USER_BUSSINESS_GOAL 
        SET GOAL=?
        WHERE USER_ID=? AND DATE_MONTH=EXTRACT(MONTH FROM SYSDATE()) 
        AND DATE_YEAR=EXTRACT(YEAR FROM SYSDATE());
    ');
    $stmt->execute([$goal,$id]);
        }
        catch(PDOException $e)
        {
            echo "cos poszlo nie tak";    
        }
    $stmt = null;
}
public function patchpass(string $id,string $pass){
    
    try {
        $stmt = $this->database->connect()->prepare('
        UPDATE users 
        SET password=?
        WHERE id=?;
    ');
    $stmt->execute([$pass,$id]);
        }
        catch(PDOException $e)
        {
            echo "cos poszlo nie tak";    
        }
    $stmt = null;
}
public function session_control(int $id,STRING $session): bool {
    $stmt = $this->database->connect()->prepare('
    select CLEANING();
');
$stmt->execute();
    
    $stmt = $this->database->connect()->prepare('
    SELECT SESSION_,EXPIRATION_TIME FROM SESSION_CONTROL WHERE USER_ID=?;
');
$stmt->execute([$id]);
$ses=$stmt->fetch(PDO::FETCH_ASSOC);
if($ses== false){
    return false;
}

if($ses['SESSION_']==$session)
{
return true;}
else 
{
    return false;
}
}


public function save_session(int $id,string $session){
    $stmt = $this->database->connect()->prepare('
    INSERT INTO SESSION_CONTROL (USER_ID,SESSION_,EXPIRATION_TIME) VALUES (?,?,DATE_ADD(SYSDATE(),INTERVAL 1 HOUR));
');
$stmt->execute([$id,$session]);
}
public function spending_calc(int $id):array {
    $stmt = $this->database->connect()->prepare('
    SELECT EXP,GOAL FROM EXPENSES WHERE id=?;
    ');
    $stmt->execute([$id]);
    $ses=$stmt->fetch(PDO::FETCH_ASSOC);
    if($ses == false){
        $stmt = $this->database->connect()->prepare('
        INSERT INTO USER_BUSSINESS_GOAL (USER_ID,GOAL,DATE_MONTH,DATE_YEAR) VALUES (?,\'1000\' ,EXTRACT(MONTH FROM SYSDATE()),EXTRACT(YEAR FROM SYSDATE()));  
        ');
        $stmt->execute([$id]);
        $stmt = $this->database->connect()->prepare('
        INSERT INTO USER_EXPENSES (USER_ID,DATE_EXP,VALUE_EXP,COMMENT) VALUES (?,SYSDATE(),\'0\',"TEST_COMMENT");  
        ');
        $stmt->execute([$id]);
    }
    
    $stmt = $this->database->connect()->prepare('
        SELECT EXP,GOAL FROM EXPENSES WHERE id=?;
        ');
        $stmt->execute([$id]);
        $ses=$stmt->fetch(PDO::FETCH_ASSOC);

    return $ses ;
}
public function addexp(string $value, string $comm){
    $value=floatval($value);
    try {
        $stmt = $this->database->connect()->prepare('
        INSERT INTO USER_EXPENSES 
        (USER_ID,DATE_EXP,VALUE_EXP,COMMENT) VALUES (?,SYSDATE(),?,?);
    ');
    $stmt->execute([$_SESSION['USER_ID'],$value,$comm]);
        }
        catch(PDOException $e)
        {
            echo "cos poszlo nie tak";    
        }
    $stmt = null;
}
public function log(string $seg,string $action){
    try {
        $stmt = $this->database->connect()->prepare('
        INSERT INTO LOG 
        (USER_ID,SEGMENT_,ACTION,ACTION_DATE) VALUES (?,?,?,SYSDATE());
    ');
    $stmt->execute([$_SESSION['USER_ID'],$seg,$action]);
        }
        catch(PDOException $e)
        {
            echo "cos poszlo nie tak podczas robienia log, zgłoś problem do administratora";    
        }
    $stmt = null;
}
public function logr(string $seg,string $action,string $email){
    $stmt = $this->database->connect()->prepare('
            Select id from users where email=?;
        ');
        $stmt->execute([$email]);
        $users = $stmt->fetch(PDO::FETCH_ASSOC);
        $id = $users['id'];
    
    try {
        $stmt = $this->database->connect()->prepare('
        INSERT INTO LOG 
        (USER_ID,SEGMENT_,ACTION,ACTION_DATE) VALUES (?,?,?,SYSDATE());
    ');
    $stmt->execute([$id,$seg,$action]);
        }
        catch(PDOException $e)
        {
            echo "cos poszlo nie tak podczas robienia log, zgłoś problem do administratora";    
        }
    $stmt = null;
}

public function delatebyid(int $id){
    $stmt = $this->database->connect()->prepare('
    DELETE FROM users WHERE id=?;
');
$stmt->execute([$id]);
}
}