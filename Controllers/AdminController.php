<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//User.php';
require_once __DIR__.'//..//Repository//UserRepository.php';

class AdminController extends AppController {

    public function index()
    {
        $userRepository = new UserRepository();
        if(isset($_SESSION['id'])){
        $this->render('users', ['user' => $userRepository->getUser($_SESSION['id']),'db'=>new UserRepository()]);
    }}

    public function users()
    {   
        $userRepository = new UserRepository();
        
        header('Content-type: application/json');
        http_response_code(200);
        
        $users = $userRepository->getUsers();
        echo $users ? json_encode($users) : '';
        $userRepository->log("AdminController","take all user list");
    }
    public function delatebyid(){
        $userRepository = new UserRepository();
        if ($this->isPost()) {
            $id = $_POST['id'];
        $userRepository->delatebyid($id);
        $this->render('users', ['messages'=>['User deleted'],'user' => $userRepository->getUser($_SESSION['id']),'db'=>new UserRepository()]);}
    }
}