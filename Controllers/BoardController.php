<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//Post.php';
require_once __DIR__.'//..//Repository//UserRepository.php';
require_once __DIR__.'//..//Database.php';

class BoardController extends AppController {

    public function yourboard()
    {   
        $userRepository = new UserRepository();
        if(isset($_SESSION['id'])){
        $this->render('board',['user' => $userRepository->getUser($_SESSION['id']),'db'=>new UserRepository()]);}
        else {$url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=login");}
    }
    public function index()
    {
        $userRepository = new UserRepository();
        if(isset($_SESSION['id'])){
        $this->render('addexpenses', ['user' => $userRepository->getUser($_SESSION['id']),'db'=>new UserRepository()]);
    }}

    public function addexp()
    {
        if ($this->isPost()) {
            $value = $_POST['value'];
            $comment = $_POST['comment'];
            $userRepository = new UserRepository();
            $userRepository->addexp($value,$comment);
            $userRepository->log("BoardController","add expenses");
        if(isset($_SESSION['id'])){
        $this->render('board',['messages' => ['Add expenses complete!'],'user' => $userRepository->getUser($_SESSION['id']),'db'=>new UserRepository()]);}
        }
       
    }

}