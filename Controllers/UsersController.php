<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//User.php';
require_once __DIR__.'//..//Repository//UserRepository.php';


class UsersController extends AppController {
    public function index()
    {
        $userRepository = new UserRepository();
        if(isset($_SESSION['id'])){
        $this->render('userpanel', ['user' => $userRepository->getUser($_SESSION['id']),'db'=>new UserRepository()]);
    }}
    public function index2()
    {
        $userRepository = new UserRepository();
        if(isset($_SESSION['id'])){
        $this->render('usergoal', ['user' => $userRepository->getUser($_SESSION['id']),'db'=>new UserRepository()]);
    }}
    public function index3()
    {
        $userRepository = new UserRepository();
        if(isset($_SESSION['id'])){
        $this->render('userpassword', ['user' => $userRepository->getUser($_SESSION['id']),'db'=>new UserRepository()]);
    }}
   public function patchdata()
   {
    $userRepository=new UserRepository();
    if ($this->isPost()) {
        $email = $_POST['email'];
        $name = $_POST['name'];
        $surname = $_POST['surname'];
        $age = $_POST['age'];
        $city = $_POST['city'];
        $profession = $_POST['profession'];
        $phone = $_POST['phone'];
        $nick = $_POST['nick'];
        $userRepository->patchdate($_SESSION['USER_ID'],$email,$name,$surname,$age,$city,$profession,$phone,$nick);
        $userRepository->log("UsersController","patch data");
    }
    $usero=$userRepository->getUser($_SESSION['id']);
    $usero->setEmail($email);
    $usero->setName($name);
    $usero->setSurname($surname);
    $usero->setAge($age);
    $usero->setCity($city);
    $usero->setProfession($profession);
    $usero->setPhone($phone);
    $usero->setNick($nick);
    if(isset($_SESSION['id'])){
    $this->render('user_contr',['messages'=>['Update finish!'],'user' => $userRepository->getUser($_SESSION['id']),'db'=>new UserRepository()]);
      }}
      public function wait(){

        $userRepository = new UserRepository();
        if(isset($_SESSION['id'])){
        $this->render('user_contr',['user' => $userRepository->getUser($_SESSION['id']),'db'=>new UserRepository()]);
      }}
      public function patchgoal()
      {
       $userRepository=new UserRepository();
       if ($this->isPost()) {
           $goal = $_POST['goal'];
           $userRepository->patchgoal($_SESSION['USER_ID'],$goal);
           $userRepository->log("UsersController","patch goal");
       }
       if(isset($_SESSION['id'])){
       $this->render('user_contr',['messages'=>['Update finish!'],'user' => $userRepository->getUser($_SESSION['id']),'db'=>new UserRepository()]);
         
      }}
      public function patchpass()
      {
        $userRepository=new UserRepository();
        $user = $userRepository->getUser($_SESSION['id']);
        if ($this->isPost()) {
          $pass1 = $_POST['pass1'];
          $pass2 = $_POST['pass2'];
          $pass3 = $_POST['pass3'];
          if(!password_verify($pass1, $user->getPassword())) {
            if(isset($_SESSION['id'])){
            $this->render('userpassword', ['messages'=>['Wrong old password'],'user' => $userRepository->getUser($_SESSION['id']),'db'=>new UserRepository()]);
            return;
            }
          }
            else
            {
          if($pass1==$pass2){
            $pass3=password_hash($pass3, PASSWORD_DEFAULT);
          $userRepository->patchpass($_SESSION['USER_ID'],$pass3);
          $userRepository->log("UsersController","patch password");
          }
          else
          {
            if(isset($_SESSION['id'])){
            $this->render('userpassword',['messages'=>['Wrong old password'],'user' => $userRepository->getUser($_SESSION['id']),'db'=>new UserRepository()]);
            }
          }

          if(isset($_SESSION['id'])){
            $this->render('user_contr',['messages'=>['Update finish!'],'user' => $userRepository->getUser($_SESSION['id']),'db'=>new UserRepository()]); 
           }
      }}


      }
   }




