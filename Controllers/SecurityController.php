<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//User.php';
require_once __DIR__.'//..//Repository//UserRepository.php';

class SecurityController extends AppController {

    public function login()
    {   
        $userRepository = new UserRepository();

        if ($this->isPost()) {
            $email = $_POST['email'];
            $password = $_POST['password'];
            //$hash=password_hash('admin', PASSWORD_DEFAULT);
            //$userRepository->patchpass('1',$hash);
            $user = $userRepository->getUser($email);

            if (!$user) {
                $this->render('login', ['messages' => ['User with this email not exist!']]);
                return;
            }
            if (!password_verify($password, $user->getPassword())) {
                $this->render('login', ['messages' => ['Wrong password!']]);
                return;
            }
            

            $_SESSION["id"] = $user->getEmail();
            $_SESSION["role"] = $user->getRole();
            $_SESSION["USER_ID"] = $user->getId();
            $userRepository->save_session($_SESSION["USER_ID"],implode($_SESSION));
            $userRepository->log("SecurityController","Log in");    
            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=board");
            return;
        }

        $this->render('login');
    }

    public function logout()
    {
        $userRepository = new UserRepository();
        $userRepository->log("SecurityController","Log out");
        session_unset();
        session_destroy();

        $this->render('login', ['messages' => ['You have been successfully logged out!']]);
    }
    public function registration(){
        $userRepository = new UserRepository();
    if ($this->isPost()) 
    {
        $email = $_POST['email'];
        $password =$_POST['password'];
        $password2 =$_POST['password2'];
        $name =$_POST['name'];
        $surname =$_POST['surname'];


        $user = $userRepository->getUser($email);
        if($email!=""&&$password!="")
        {
            if (!$user)
        {
            if ($password == $password2)
            {
                $hash=password_hash($password, PASSWORD_DEFAULT);
                if(filter_var($email, FILTER_VALIDATE_EMAIL)){
                $userRepository->newUsers($email, $hash, $name,$surname);
                $userRepository->logr("SecurityController","Reginster",$email);
                $this->render('login', ['messages' => ['account was created']]);
                return;}
                else{$this->render('registration', ['messages' => ['type correct email adress']]);
                    return;}
            }
            else {
                $this->render('registration', ['messages' => ['Incorrect password2']]);
                return;
            }
        }
        else {
            $this->render('registration', ['messages' => ['User with this email exist!']]);
            return;
        }
        }
        else{ $this->render('registration', ['messages' => ['Fill mandatory fild!']]);
            return;}
        }
        $this->render('registration');
    }
}