<?php

class User {
    private $id;
    private $email;
    private $password;
    private $name;
    private $surname;
    private $role = ['ROLE_USER'];
    private $age;
    private $city;
    private $profession;
    private $phone;
    private $nick;

    public function __construct(
        string $email,
        string $password,
        string $name="0",
        string $surname="0",
        int $id = null,
        string $role,
        string $age="0",
        string $city="0",
        string $profession="0",
        string $phone="0",
        string $nick="0"
    ) {
        $this->email = $email;
        $this->password = $password;
        $this->name = $name;
        $this->surname = $surname;
        $this->id = $id;
        $this->role=[1 => 'ROLE_USER',
                     2 => $role];
        $this->age=$age;
        $this->city=$city;
        $this->profession=$profession;
        $this->phone=$phone;
        $this->nick=$nick;

    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getEmail(): string  
    {
        return $this->email;
    }
    public function setEmail(string $email)  
    {
        $this->email=$email;
    }

    public function getRole(): array
    {
        return $this->role;
    }

    public function getName(): string
    {
        return $this->name;
    }
    public function setName(string $name)
    {
        $this->name=$name;
    }
    public function getSurname(): string
    {
        return $this->surname;
    }
    public function setSurname(string $surname)
    {
        $this->surname=$surname;
    }
    public function getAge(): string
    {
        return $this->age;
    }
    public function setAge(string $age)
    {
        $this->age=$age;
    }
    public function getCity(): string
    {
        return $this->city;
    }
    public function setCity(string $city)
    {
        $this->city=$city;
    }
    public function getProfession(): string
    {
        return $this->profession;
    }
    public function setProfession(string $profession)
    {
        $this->profession=$profession;
    }
    public function getPhone(): string
    {
        return $this->phone;
    }
    public function setPhone(string $phone)
    {
        $this->phone=$phone;
    }
    public function getNick(): string
    {
        return $this->nick;
    }
    public function setNick(string $nick)
    {
        $this->nick=$nick;
    }

}