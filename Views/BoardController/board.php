<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
    if(!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
        $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=login");
    }

    if(!in_array('ROLE_USER', $_SESSION['role'])) {
        if(!in_array('admin', $_SESSION['role'])){
        $url = "http://$_SERVER[HTTP_HOST]/";
        header("Location: {$url}?page=login");}
    }

    if(!$db->session_control($_SESSION['USER_ID'],implode($_SESSION))){
        $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=login");
    }

?>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="Stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.css" />
    <link rel="Stylesheet" type="text/css" href="../Public/css/style.css" />

    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

    <?php include(dirname(__DIR__).'/Common/head.php'); ?>
    <title>Taxawo</title>
</head>
<body>
<?php
if(in_array('admin', $_SESSION['role'])){
include(dirname(__DIR__).'/Common/navbar_admin.php'); }
else {
include(dirname(__DIR__).'/Common/navbar.php');
}
$num1=$db->spending_calc($_SESSION['USER_ID']);
?>
<div class="container">
<div id='zxc'>
<canvas id="myChart" width="600" height="600"></canvas>
<script>
var number = "<?php echo $num1['GOAL'] ?>";
var number2 = "<?php echo $num1['EXP'] ?>";
var number3 = number-number2;
if (number3<-1){
number3=0;
alert('You have reached the spending limit!!!')

}
var ctx = document.getElementById('myChart');
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ['how much left to limit', 'Expenses'],
        datasets: [{
            label: '# of Votes',
            data: [number3,number2],
            backgroundColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)'
            ],

            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: false
                }
            }]
        }
    }
});
</script>
</div> 
    <div class="flex" id='asdqwe'>  
        <div class="messages">
            <?php
         if(isset($messages)){
            foreach($messages as $message) {
            echo $message;
        }}?>
        </div > 
        <button type="button" onclick="parent.location='?page=addexpenses'">ADD EXPENSES</button>
        <button type="button" onclick="parent.location='?page=usergoal'">EDIT YOUR GOAL</button>
        </div>
</div>
</body>
</html>