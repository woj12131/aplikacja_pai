<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
    if(!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
        $url = "http://$_SERVER[HTTP_HOST]/";
        header("Location: {$url}?page=login");
    }

    if(!in_array('ROLE_USER', $_SESSION['role'])) {
        if(!in_array('admin', $_SESSION['role'])){
            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=login");
    }}

    if(!$db->session_control($_SESSION['USER_ID'],implode($_SESSION))){
        $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=login");
    }

?>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="Stylesheet" type="text/css" href="../Public/css/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <?php include(dirname(__DIR__).'/Common/head.php'); ?>
    <title>Taxawo</title>
</head>
<body>
<?php if(in_array('admin', $_SESSION['role'])){
include(dirname(__DIR__).'/Common/navbar_admin.php'); }
else {
include(dirname(__DIR__).'/Common/navbar.php');
} ?>
<div class="container">
    <div class="logo">
        <img src="../Public/img/taxawo.svg">
    </div>       
    <form action="?page=addexp" method="POST" id="formd">
        <div class="messages">
        <?php
        if(isset($messages)){
        foreach($messages as $message) {
        echo $message;
        }}?>
        </div>
        <div class="flex" id="addexp">  
        <input name="value" type="text" placeholder="Expenses value" required>
        <textarea name="comment" form="formd" rows="2" cols="4" placeholder="write a comment......" ></textarea>
        <button type="submit">SAVE YOURS EXPENSES</button>
        </div>

    </form>
</div>
</body>
</html>