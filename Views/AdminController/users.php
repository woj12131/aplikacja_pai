<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
    if(!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
        die('You are not logged in!');
        $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=board");
    }
    if(!in_array('admin', $_SESSION['role'])) {
        die('You do not have permission to watch this page!');
    }

    if(!$db->session_control($_SESSION['USER_ID'],implode($_SESSION))){
        die('your session has expired!');
        $url = "http://$_SERVER[HTTP_HOST]/";
        header("Location: {$url}?page=board");
    }

?>


<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="Stylesheet" type="text/css" href="../Public/css/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <?php include(dirname(__DIR__).'/Common/head.php'); ?>
    <title>Taxawo</title>
</head>
<body>
<?php if(in_array('admin', $_SESSION['role'])){
include(dirname(__DIR__).'/Common/navbar_admin.php'); }
else {
include(dirname(__DIR__).'/Common/navbar.php');
} ?>
<div class="container">
<div class="col-6">
<div class="messages">
            <?php
                if(isset($messages)){
                    foreach($messages as $message) {
                        echo $message;
                    }
                }
            ?></div>
    <table class="table mt-4 text-light">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Email</th>
            <th scope="col">Name</th>
            <th scope="col">Surname</th>
            <th scope="col">role</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <th scope="row"><?= $user->getId(); ?></th>
            <td><?= $user->getEmail(); ?></td>
            <td><?= $user->getName(); ?></td>
            <td><?= $user->getSurname(); ?></td>
            <td><?= $user->getRole()[2]; ?></td>
            </tr>
        </tbody>
        <tbody class="users-list">
        </tbody>
    </table>
    <form action="?page=delatebyid" method="POST" class="flex" id="asd">
    <button  type="button" onclick="getUsers()">
        Get all users    
    </button>
    <div class="flex2">
    <input name="id" type="number" placeholder="nr" >
    <button  type="submit">Delete user with id</button></div>   
</form>
</div>
</div>
</body>
</html>