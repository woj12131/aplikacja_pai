<nav class="navbar navbar-expand navbar-dark bg-dark">
    
    <div class=".navbar-nav" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="?page=board">
                    <i class="fas fa-snowboarding"></i> Board
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="?page=user_contr">
                <i class="fas fa-user"></i> User Panel 
                </a>
            </li>
            <li class="nav-item">
                <a class=""></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="?page=logout">
                    <i class="fas fa-sign-out-alt"></i> Logout
                </a>
            </li>

        </ul>      
    </div>
</nav>