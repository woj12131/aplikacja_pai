<nav class="navbar navbar-expand navbar-dark bg-dark">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="?page=board">
                    <i class="fas fa-snowboarding"></i> Board
                </a>
            </li>
            <li class="nav-item" id="adminp">
                <a class="nav-link" href="?page=admin">
                    <i class="fas fa-users"></i> Admin panel
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="?page=logout">
                    <i class="fas fa-sign-out-alt"></i> Logout
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="?page=user_contr">
                <i class="fas fa-user"></i> User Panel 
                </a>
            </li>
        </ul>      
 
</nav>

