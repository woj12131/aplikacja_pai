<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="Stylesheet" type="text/css" href="../Public/css/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <?php include(dirname(__DIR__).'/Common/head.php');?>
    <title>Taxawo</title>
</head>
<body>
<div class="container">
    <div class="logo">
        <img src="../Public/img/Taxawo.svg">
    </div>
        <form action="?page=login" method="POST">
            <div class="messages">
            <?php
                if(isset($messages)){
                    foreach($messages as $message) {
                        echo $message;
                    }
                }
            ?>
            </div>
        <input name="email" type="text" placeholder="email@email.com" value="admin">
        <input name="password" type="password" placeholder="password" value="admin">
        <button type="submit">CONTINUE</button>
        <button type="button" onclick="parent.location='?page=registration'">REGISTRATION</button>
    </form>

</div>
</body>
</html>