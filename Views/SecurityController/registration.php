
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="Stylesheet" type="text/css" href="../Public/css/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <?php include(dirname(__DIR__).'/Common/head.php'); ?>
    <title>Taxawo</title>
</head>
<body onload='document.form2.email.focus()'>
<div class="container">
    <div class="logo">
    <img src="../Public/img/Taxawo.svg">
    </div>
    <form name="form2" action="?page=registration" method="POST">
        <div class="messages">
            <?php
                if(isset($messages)){
                    foreach($messages as $message) {
                        echo $message;
                    }
                }
            ?>
        </div>
        <input id='email' name="email" type="text" placeholder="email@email.com" required>
        <input name="password" type="password" placeholder="password" required>
        <input name="password2" type="password" placeholder="repeat password" required>
        <input name="name" type="text" placeholder="name">
        <input name="surname" type="text" placeholder="surname">
        <button type="submit"id='validate'>CONTINUE</button>
    </form>
</div>
</body>
<script>


function validate() {
  var $result = $("#result");
  var email = $("#email").val();
  $result.text("");

  if (validateEmail(email)) {
    $result.text(email + " is valid :)");
    $result.css("color", "green");
  } else {
    $result.text(email + " is not valid :(");
    $result.css("color", "red");
  }
  return false;
}
$("#validate").on("click", validate);

</script> </html>